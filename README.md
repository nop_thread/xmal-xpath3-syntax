# xmal-xpath3-syntax

[![Build Status](https://gitlab.com/nop_thread/xmal-xpath3-syntax/badges/develop/pipeline.svg)](https://gitlab.com/nop_thread/xmal-xpath3-syntax/pipelines/)
<!--
[![Latest version](https://img.shields.io/crates/v/xmal-xpath3-syntax.svg)](https://crates.io/crates/xmal-xpath3-syntax)
[![Documentation](https://docs.rs/xmal-xpath3-syntax/badge.svg)](https://docs.rs/xmal-xpath3-syntax)
-->
![Minimum supported rustc version: 1.57](https://img.shields.io/badge/rustc-1.57+-lightgray.svg)

XPath 3.1 syntax tree and parser.

## License

Licensed under either of

* Apache License, Version 2.0, ([LICENSE-APACHE.txt](LICENSE-APACHE.txt) or
  <https://www.apache.org/licenses/LICENSE-2.0>)
* MIT license ([LICENSE-MIT.txt](LICENSE-MIT.txt) or
  <https://opensource.org/licenses/MIT>)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
